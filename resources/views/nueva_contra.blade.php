<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nueva Contrase&ntilde;a</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

     <script src='https://www.google.com/recaptcha/api.js'></script>
     <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
     <link rel="stylesheet" href="css/cssNContra.css">

</head>
<body>
    <header>
        <div>
            <img src="img/logo-itc.svg" alt="">
        </div>
        <form>
            <div>
                <span > Nueva Contrase&ntilde;a </span>
                <input type="password" placeholder="Nueva Contrase&ntilde;a">
            </div>
            <div>
                <span>Repite Contrase&ntilde;a</span>
                <input type="password" placeholder="Repite Contrase&ntilde;a">
            </div>

           <!-- <div class="g-recaptcha" data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div> -->
            <div>
                <input class="btn btn-secondary" type="submit" value="Cambiar Contrase&ntilde;a">
            </div>
        </form>

    </header>

</body>
</html>
