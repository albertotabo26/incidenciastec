<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

     <link rel="stylesheet" href="css/estilo2.css">

    <title>Reportes</title>
</head>
<body>
<header>
        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #1b396a;">
            <a class="navbar-brand" href="#">
              <img src="img/logo-itc.svg" width="45" height="45" alt="" loading="lazy">
            </a>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                  <a class="nav-link" href="/inicio" style="color: white;">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" style="color: white;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Usuarios
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="registrou">Registro Usuarios</a>
                    <a class="dropdown-item" href="/usuarios">Inf. Usuarios</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" style="color: white;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Incidencias
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="altain">Alta Incidencias</a>
                    <a class="dropdown-item" href="/asignar">Asignar Incidencia</a>
                    <a class="dropdown-item" href="/infoi">Inf. Incidencias</a>
                </div>
            </li>
                <li class="nav-item active">
                  <a class="nav-link" href="#" style="color: white;">Equipos</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="registrod" style="color: white;">Registro Dep.</a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="Reportes" style="color: white;">Reportes</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Buscar</button>
            </form>
          </div>
        </nav>
    </header>
    <span style="padding-left: 100px; font-size: 2.5em;">Reportes</span>

    <table class="table table-striped" style="width: 80%; text-align: center; margin-left: auto; margin-right: auto;">
        <thead class="table-primary">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nombre tecnico</th>
              <th scope="col">Incidencias pendientes</th>
              <th scope="col">Incidencias liberadas</th>
              <th scope="col">Incidencias finalizadas</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <th scope="row">ID</th>
                <td>Noel Inzunza</td>
                <td>2</td>
                <td>5</td>
                <td>8</td>
                <td>15</td>
            </tr>
            <tr>
                <th scope="row">ID</th>
                <td>Marco Lopéz</td>
                <td>1</td>
                <td>5</td>
                <td>7</td>
                <td>13</td>
            </tr>
            <tr>
                <th scope="row">ID</th>
                <td>Luis Bolaños</td>
                <td>2</td>
                <td>7</td>
                <td>7</td>
                <td>16</td>
            </tr>
    </table>