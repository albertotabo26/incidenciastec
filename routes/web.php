<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/usuarios',"usuarios");
Route::view('/registrou',"registrar_usuario");
Route::view('infoi',"apartado_incidencias");
Route::view('incidencias',"incidencias");
Route::view('inicio',"inicio");
Route::view('registrod',"registrodep");
Route::view('asignar',"asignar_incidencia");
Route::view('altain',"alta_incidencia");
Route::view('Reportes', "Reportes");
Route::get('/', function () {
    return view('incidencias');
});
